package basic;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest4 {

	@Test
	public void restAssuredGet() {

		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/10");
		String firstName = response.jsonPath().get("firstname").toString();
		System.out.println(firstName);

		String firstName1 = response.jsonPath().getString("firstname");
		System.out.println(firstName1);
		
		Object obj = response.jsonPath().get("firstname");
		if(obj instanceof String) {
			
			System.out.println(obj.toString());
		}
		else if (obj instanceof Integer) {
			
			int i=(Integer)obj;
			System.out.println(i);
			
		}
	}

}
