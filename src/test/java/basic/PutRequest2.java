package basic;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PutRequest2 {
	
	private static String token=null;	
	
	@Test(priority = 1)
	public void generateToken() {
		
		token = RestAssured.given()
				.log()
				.all()
		.contentType(ContentType.JSON)
		.baseUri("https://restful-booker.herokuapp.com/")
		.body("{\r\n"
				+ "    \"username\" : \"admin\",\r\n"
				+ "    \"password\" : \"password123\"\r\n"
				+ "}")
		.when()
		.post("auth")
		
		.then()
		.log()
		.all()
		.statusCode(200)
		.extract()
		.jsonPath()
		.getString("token");
	}

	@Test(priority = 2)
	public void updateBooking() {

		RestAssured.given().log().all().contentType(ContentType.JSON)
		
		.header("Cookie","token="+token).baseUri("https://restful-booker.herokuapp.com/")
				.body("{\r\n" + "    \"firstname\" : \"Manish\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
						+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2018-01-01\",\r\n"
						+ "        \"checkout\" : \"2019-01-01\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"Breakfast\"\r\n" + "}")

				.when().put("booking/1")

				.then().log().all().statusCode(200);

	}

}
