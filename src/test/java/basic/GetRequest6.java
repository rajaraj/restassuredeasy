package basic;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRequest6 {

	@Test
	public void restAssuredGet() {

		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/10");
		// System.out.println(response.asString());
		// System.out.println(response.toString());
		// System.out.println(response.jsonPath().get("firstname").toString());
		JsonPath jsonPath = response.then().body("firstname", Matchers.equalTo("Sally")).statusCode(200).extract().jsonPath();
		System.out.print(jsonPath.getString("firstname"));
	}

}
