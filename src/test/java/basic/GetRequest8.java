package basic;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class GetRequest8 {

	@Test
	public void restAssuredGet() {

		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/10");

		Headers headers = response.getHeaders();

		for (Header h : headers) {

			System.out.println(h.getName() + "  : " + h.getValue());
		}
		
		String header = response.getHeader("Connection");
		System.out.println("--------");
		System.out.println(header);

	}

}
