package basic;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Test1 {

	@Test
	public void restAssuredGet() {

		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/10");
		response.prettyPrint();
		//System.out.println(response.asString());
		JsonPath jsonPath = response.then().body("bookingdates.checkin", Matchers.equalTo("2016-07-30")).extract()
				.jsonPath();
		System.out.println(jsonPath);

	}

}
