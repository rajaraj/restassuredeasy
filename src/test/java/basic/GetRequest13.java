package basic;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest13 {

	@Test
	public void restAssuredGet() {

		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking").then().body("size()", Matchers.is(47))
				.extract().response();
		
		int i = response.jsonPath().getList("$").size();
		System.out.println(i);

	}

}
