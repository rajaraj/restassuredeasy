package basic;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest10 {

	@Test
	public void restAssuredGet() {

		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking");
		response.prettyPrint();
		
		int id = response.jsonPath().getInt("[5].bookingid");
		System.out.println(id);

	}

}
