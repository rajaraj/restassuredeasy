package basic;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest2 {

	@Test
	public void restAssuredGet() {

		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/10");
		System.out.println(response.statusCode());
		Assert.assertEquals(200, 200);

		System.out.println(response.getStatusCode());

		String line = response.getStatusLine();
		Assert.assertEquals(line, "HTTP/1.1 200 OK");
		System.out.println(line);

	}

}
