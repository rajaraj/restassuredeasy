package basic;

import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class GetRequest5 {

	@Test
	public void restAssuredGet() {

		String firstName = RestAssured.get("https://restful-booker.herokuapp.com/booking/10").then().statusCode(200)
				.statusLine("HTTP/1.1 200 OK").extract().jsonPath().getString("firstname");
		System.out.println(firstName);
	}

}
