package payLoadHandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class NestedJsonPayLoad {

	@Test
	public void createdNestedPayLoad() {

		Map<String, Object> skills1 = new LinkedHashMap();
		skills1.put("Subject", "java");
		skills1.put("exp", 5);
		skills1.put("proficiency", "Intermediate");

		Map<String, Object> skills2 = new LinkedHashMap();
		skills2.put("Subject", "Selenium");
		skills2.put("proficiency", "Intermediate");

		List<Map<String, Object>> allSkills = new ArrayList();
		allSkills.add(skills1);
		allSkills.add(skills2);

		List<String> mob = new ArrayList<String>();
		mob.add("876987123");
		mob.add("789423734");

		Map<String, Object> finalPayLoad = new HashMap();
		finalPayLoad.put("skills", allSkills);
		finalPayLoad.put("mobile", mob);

		RestAssured.given().log().all().contentType(ContentType.JSON).body(finalPayLoad).baseUri("https://restful-booker.herokuapp.com/").when().post("booking").then().log().all()
				.statusCode(200);

	}

}
