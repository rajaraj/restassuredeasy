package payLoadHandling;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PayLoadAsMap {

	@Test
	public void PayLoadAsString() {

		Map<String, String> addressMap2 = new LinkedHashMap();
		addressMap2.put("street", "purab sarai fari");
		addressMap2.put("city", "Munger");
		addressMap2.put("state", "Bihar");

		Map<String, Object> emp1 = new LinkedHashMap();
		emp1.put("name", "Rohit");
		emp1.put("sub", "Api Testing");
		emp1.put("address", addressMap2);

		Map<String, String> addressMap1 = new LinkedHashMap();
		addressMap1.put("street", "noida sector");
		addressMap1.put("city", "Noida");
		addressMap1.put("state", "U.P");

		Map<String, Object> emp2 = new LinkedHashMap();
		emp2.put("name", "Sohan");
		emp2.put("sub", "Java Testing");
		emp2.put("address", addressMap1);

		List<Map<String, Object>> allEmps = new ArrayList();
		allEmps.add(emp1);
		allEmps.add(emp2);

		RestAssured.given().log().all().contentType(ContentType.JSON).baseUri("https://restful-booker.herokuapp.com/")
				.body(allEmps).when().post("booking").then().log().all().statusCode(200);

	}

}
