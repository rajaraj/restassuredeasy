package basic;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class DeleteRequest {
	
	private static String token=null;	
	
	@Test
	public void generateToken() {
		
		token = RestAssured.given()
				.log()
				.all()
		.contentType(ContentType.JSON)
		.baseUri("https://restful-booker.herokuapp.com/")
		.body("{\r\n"
				+ "    \"username\" : \"admin\",\r\n"
				+ "    \"password\" : \"password123\"\r\n"
				+ "}")
		.when()
		.post("auth")
		
		.then()
		
		.statusCode(200)
		.extract()
		.jsonPath()
		.getString("token");
	}

	
	@Test
	public void updatBooking() {


		RestAssured.given().log().all().contentType(ContentType.JSON)
		
		.header("Cookie","token="+token).baseUri("https://restful-booker.herokuapp.com/")
				

				.when().delete("booking/1")

				.then().log().all().statusCode(201);

}
}
